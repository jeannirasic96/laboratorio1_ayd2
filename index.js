'use strict';
require('dotenv').config();

var logger = require('morgan');
const express = require('express');
const port = process.env.PORT || 3000;
const path = require('path');

var index = require('./src/routes/index');
const app = express();

app.use(logger('dev'));
app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, '/src/views'));
app.use(express.static(path.join(__dirname, '/src/public'))); //Indica donde estan los archivos de css y eso
app.use('/', index);

app.listen(port, function(){
    console.log(`El servidor esta corriendo en el puerto ${port}`);
});

